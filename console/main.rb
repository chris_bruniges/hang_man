# frozen_string_literal: true

require './dependencies'

dependencies = Dependencies.new
view = dependencies.welcome_view

view.welcome
name = view.ask_name

dependencies.game_controller(name).run
