# frozen_string_literal: true

# gameView_spec.rb
require './console/game/game_view'

describe GameView do
  subject(:view) { GameView.new(strings, output: output, input: input) }
  let(:strings) { 'hello' }
  let(:output_char) { 'a' }
  let(:output) { double }
  let(:gets) { double }
  let(:input) { double }
  let(:cur_result) { '' }
  let(:guess) { 'a' }
  let(:quit_magic_word) { 'quit' }

  def assert_puts(string)
    expect(output).to receive(:puts).with(string)
  end

  def setup_strings_for_result(cur_result, answer)
    { 'guessResults' =>
      {
        cur_result => answer
      } }
  end

  describe '#display_user_goodbye' do
    context 'called with correct parameters' do
      let(:strings) do
        {
          'word_reveal' => 'word_reveal %s',
          'bye' => 'bye %s'
        }
      end

      it 'displays the plain word' do
        assert_puts('word_reveal aword')
        assert_puts('bye myname')
        view.display_user_goodbye('aword', 'myname')
      end
    end
  end

  describe '#display_start_round_header' do
    context 'displays when called with correct parameters' do
      let(:strings) do
        {
          'guesses_remaining' => 'guesses_remaining %s',
          'trying_to_guess' => 'trying_to_guess %s'
        }
      end
      let(:output) { double }

      it 'prints the header' do
        assert_puts('trying_to_guess **ad*')
        assert_puts('guesses_remaining 4')
        view.display_start_round_header(4, '**ad*')
      end
    end
  end

  describe '#input_guess_from_user' do
    context 'when the user is ready to enter something' do
      let(:strings) do
        {
          'prompt' => 'prompt'
        }
      end

      it 'returns value from user input' do
        assert_puts('prompt')
        expect(input).to receive(:gets).and_return(gets)
        expect(gets).to receive(:chomp).and_return(output_char)
        expect(view.input_guess_from_user).to eq(output_char)
      end
    end

    context 'when the user exits using ctrl+c' do
      let(:strings) do
        {
          'prompt' => 'prompt',
          'quit' => quit_magic_word
        }
      end

      it 'returns quit value instead' do
        assert_puts('prompt')
        expect(input).to receive(:gets).and_return(gets)
        expect(gets).to receive(:chomp).and_raise(Interrupt)
        expect(view.input_guess_from_user).to eq(quit_magic_word)
      end
    end
  end

  describe '#display_result' do
    context 'when passed a CORRECT result' do
      let(:cur_result) { GuessResult::CORRECT }
      let(:strings) { setup_strings_for_result(cur_result, 'CORRECT %s') }

      it 'displays a message' do
        assert_puts("CORRECT #{guess}")
        view.display_result(cur_result, guess)
      end
    end
    context 'when passed a INCORRECT result' do
      let(:cur_result) { GuessResult::INCORRECT }
      let(:strings) { setup_strings_for_result(cur_result, 'incorrect') }

      it 'displays a message' do
        assert_puts('incorrect')
        view.display_result(cur_result, guess)
      end
    end
    context 'when passed a INVALID result' do
      let(:cur_result) { GuessResult::INVALID }
      let(:strings) { setup_strings_for_result(cur_result, 'INVALID %s') }

      it 'displays a message' do
        assert_puts("INVALID #{guess}")
        view.display_result(cur_result, guess)
      end
    end
    context 'when passed a DUPLICATE result' do
      let(:cur_result) { GuessResult::DUPLICATE }
      let(:strings) { setup_strings_for_result(cur_result, 'DUPLICATE %s') }

      it 'displays a message' do
        assert_puts("DUPLICATE #{guess}")
        view.display_result(cur_result, guess)
      end
    end
    context 'when passed a FINISHED result' do
      let(:cur_result) { GuessResult::FINISHED }
      let(:strings) { setup_strings_for_result(cur_result, 'FINISHED') }

      it 'displays a message' do
        assert_puts('FINISHED')
        view.display_result(cur_result, guess)
      end
    end
    context 'when passed a FAILED result' do
      let(:cur_result) { GuessResult::FAILED }
      let(:strings) { setup_strings_for_result(cur_result, 'FAILED') }

      it 'displays a message' do
        assert_puts('FAILED')
        view.display_result(cur_result, guess)
      end
    end
  end
end
