# frozen_string_literal: true

# WordRepository tests
require './console/game/words_repository'

describe WordsRepository do
  subject(:repo) { WordsRepository.new(file) }
  let(:file) do
    mock = double
    allow(mock).to receive(:gets).and_return('word1', 'word2', false)
    allow(mock).to receive(:close)
    mock
  end

  describe '#random_word' do
    context 'given a list of 2 words' do
      it 'gets the words from the file' do
        expect(file).to receive(:gets).and_return('word1', 'word2', false)
        repo.random_word
      end

      it 'returns one of those words' do
        expect(repo.random_word).to be_one_of(['word1', 'word2'])
      end

      it 'closes the file' do
        expect(file).to receive(:close)
        repo.random_word
      end
    end
  end

  RSpec::Matchers.define :be_one_of do |expected|
    match do |actual|
      expected.include?(actual)
    end
  end
end
