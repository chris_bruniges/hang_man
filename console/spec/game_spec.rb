# frozen_string_literal: true

# game_spec.rb
require './console/game/game'

describe Game do
  subject(:game) { Game.new(word: word, case_sensitive: case_sensitive, repository: repo) }
  let(:word) { 'hello' }
  let(:case_sensitive) { true }
  let(:repo) { false }
  let(:repo) do
    repo = double
    allow(repo).to receive(:random_word).and_return(random_word)
    repo
  end
  let(:random_word) { 'randomly' }

  describe '#obfuscated_word' do
    context 'when you start a game' do
      it 'replaces all unguessed letters with stars' do
        expect(game.obfuscated_word).to eq('*****')
      end
    end

    context 'when you try a correct letter' do
      before do
        game.try_letter('h')
      end

      it 'de-obfuscates matching letter' do
        expect(game.obfuscated_word).to eq('h****')
      end
    end

    context 'when you try a correct letter that is *duplicated* in the word' do
      before do
        game.try_letter('l')
      end

      it 'de-obfuscates all matching letters' do
        expect(game.obfuscated_word).to eq('**ll*')
      end
    end
  end

  describe '#initialize' do
    let(:word) { false }

    it 'retrieves a word from the repo when supplied' do
      expect(game.word).to eq(repo.random_word)
    end
  end

  describe '#word' do
    it 'reveals the un-obfuscated word' do
      expect(game.word).to eq(word)
    end
  end

  describe '#quantity_guesses_remaining' do
    context 'when the letter is not there' do
      before do
        game.try_letter('k')
      end

      it 'detracts a life' do
        expect(game.quantity_guesses_remaining).to eq(4)
      end
    end

    context 'when you try a letter twice' do
      before do
        2.times do
          game.try_letter('a')
        end
      end

      it 'does not take an additional life' do
        expect(game.quantity_guesses_remaining).to eq(4)
      end
    end

    context 'when you try a correct letter' do
      before do
        game.try_letter('h')
      end

      it 'does not take a life' do
        expect(game.quantity_guesses_remaining).to eq(5)
      end
    end

    context 'when you try a non Alphabetic' do
      before do
        game.try_letter('$')
      end

      it 'does not take a life' do
        expect(game.quantity_guesses_remaining).to eq(5)
      end
    end

    context 'when you guess all the letters wrongly' do
      before do
        [*('a'..'f')].each do |letter|
          game.try_letter(letter)
        end
      end

      it 'indicates no turns remaining' do
        expect(game.quantity_guesses_remaining).to eq(0)
      end
    end
  end

  describe '#try_letter' do
    context 'when the letter is not there' do
      it 'returns INCORRECT' do
        expect(game.try_letter('k')).to eq(GuessResult::INCORRECT)
      end
    end

    context 'when you try a letter twice' do
      before do
        game.try_letter('a')
      end

      it 'returns DUPLICATE result' do
        expect(game.try_letter('a')).to eq(GuessResult::DUPLICATE)
      end
    end

    context 'when you try a correct letter' do
      it 'returns CORRECT result' do
        expect(game.try_letter('h')).to eq(GuessResult::CORRECT)
      end
    end

    context 'when you try a non Alphabetic' do
      it 'returns an INVALID result' do
        expect(game.try_letter('$')).to eq(GuessResult::INVALID)
      end
    end

    context 'when you guess all the letters correctly' do
      before do
        game.try_letter('h')
        game.try_letter('e')
        game.try_letter('l')
      end

      it 'returns FINISHED result' do
        expect(game.try_letter('o')).to eq(GuessResult::FINISHED)
      end
    end

    context 'when you guess all the letters wrongly' do
      before do
        [*('a'..'d')].each do |letter|
          game.try_letter(letter)
        end
      end

      it 'returns FAILED result' do
        expect(game.try_letter('f')).to eq(GuessResult::FAILED)
      end
    end

    context 'when caring about case for wrong letters' do
      let(:case_sensitive) { false }
      letter = 'A'
      before do
        game.try_letter(letter)
      end

      it 'returns duplicate result' do
        expect(game.try_letter(letter.downcase)).to eq(GuessResult::DUPLICATE)
      end
    end

    context 'when caring about case for right letters' do
      let(:case_sensitive) { false }

      it 'matches across cases' do
        expect(game.try_letter('H')).to eq(GuessResult::CORRECT)
      end
    end

    context 'when caring about case for duplicated right letters' do
      let(:case_sensitive) { false }
      letter = 'H'
      before do
        game.try_letter(letter)
      end

      it 'treats subsequent correct guesses as duplicates' do
        expect(game.try_letter(letter.downcase)).to eq(GuessResult::DUPLICATE)
      end
    end
  end
end
