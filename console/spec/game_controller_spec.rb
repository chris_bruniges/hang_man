# frozen_string_literal: true

# GameController tests
require './console/game/game_controller'

describe GameController do
  subject(:controller) { GameController.new(game, view, name, magic_quit_word) }
  let(:name) { 'bob' }
  let(:game) do
    instance_double('Game',
      {
      :word => word,
      :quantity_guesses_remaining => guesses,
      :obfuscated_word => obfuscated_word,
      :try_letter => guess_result
      })
  end
  let(:view) do
    instance_double('View',
      {
        :display_user_goodbye => {},
        :display_start_round_header => {},
        :display_result => {},
        :input_guess_from_user => letter,
      })
  end
  let(:guess_result) { GuessResult::FAILED }
  let(:word) { 'flobber' }
  let(:guesses) { 1 }
  let(:obfuscated_word) { 'flo**er' }
  let(:letter) { 'k' }
  let(:magic_quit_word) { 'quit' }

  describe '#run' do
    context 'when the model specifies a FAIL result' do
      it 'returns' do
        expect(game).to receive(:try_letter).with(letter).and_return(GuessResult::FAILED)

        controller.run
      end

      it 'asks for a character once only' do
        expect(game).to receive(:try_letter).with(letter).and_return(GuessResult::FAILED)
        expect(view).to receive(:input_guess_from_user).and_return(letter)

        controller.run
      end
    end

    context 'when the model specifies other result before FAIL result' do
      before do
        expect(game).to receive(:try_letter).with(letter).and_return(GuessResult::INCORRECT, GuessResult::FAILED)
      end

      it 'returns after consuming two inputs' do
        expect(view).to receive(:input_guess_from_user).and_return(letter).twice
        controller.run
      end

      it 'displays an exit message with the word and the users name' do
        expect(view).to receive(:display_user_goodbye).with(word, name).once
        controller.run
      end

      it 'displays a header each round' do
        expect(view).to receive(:display_start_round_header).with(guesses, obfuscated_word).twice
        controller.run
      end
    end

    context 'when the model specifies other result before FINISHED result' do
      before do
        expect(game).to receive(:try_letter).with(letter).and_return(GuessResult::CORRECT, GuessResult::FINISHED)
      end

      it 'returns after consuming two inputs' do
        expect(view).to receive(:input_guess_from_user).and_return(letter).twice
        controller.run
      end
    end

    context 'when we quit early' do
      before do
        allow(game).to receive(:try_letter).and_return(GuessResult::CORRECT, GuessResult::CORRECT, GuessResult::FINISHED)
      end

      it 'returns after consuming two inputs' do
        expect(view).to receive(:input_guess_from_user).and_return(letter, 'quit').twice
        controller.run
      end
    end
  end
end
