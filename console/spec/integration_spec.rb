# frozen_string_literal: true

# gameController_spec.rb
require './console/game/guess_result'
require './console/game/game'
require './console/game/game_controller'

describe GameController do
  let(:word) { 'hello' }
  let(:game) { Game.new(word: word) }
  let(:view) { GuessingView.new(guesses) }
  let(:guesses) { [] }
  let(:controller) { GameController.new(game, view, 'bob', 'quit') }

  describe '#run' do
    context 'when guessing all the right letters' do
      let(:guesses) { %w[h e non_single_character l o] }

      before do
        controller.run
      end

      it 'can win a game' do
        expect(view.last_call).to eq(GuessResult::FINISHED)
      end
    end

    context 'when guessing more wrong than lives' do
      let(:guesses) { %w[a b c d f] }
      # Configure mock to return each guess

      before do
        controller.run
      end

      it 'can lose a game' do
        expect(view.last_call).to eq(GuessResult::FAILED)
      end
    end
  end

  # Mock for tests
  class GuessingView
    attr_reader :last_call
    attr_reader :goodbye
    attr_reader :last_obfuscated

    def initialize(letters)
      @letters = letters
      @index = 0
    end

    def input_guess_from_user
      val = @letters[@index]
      @index += 1
      val
    end

    def display_user_goodbye(word, _name)
      @goodbye = word
    end

    def display_result(result, _guess)
      @last_call = result
    end

    def display_start_round_header(_quantity_guesses_remaining, obfuscated_word)
      @last_obfuscated = obfuscated_word
    end
  end
end
