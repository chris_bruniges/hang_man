# frozen_string_literal: true

# The view component for a game of hangman on the console
module Console
  class GameView
    def initialize(strings, output: STDOUT, input: STDIN)
      @output = output
      @input = input
      @strings = strings
    end

    def input_guess_from_user
      @output.puts @strings['prompt']
      begin
        guess = @input.gets.chomp
      rescue Interrupt
        guess = @strings['quit']
      end
      guess
    end

    def display_user_goodbye(word, name)
      @output.puts format(@strings['word_reveal'], word)
      @output.puts format(@strings['bye'], name)
    end

    def display_result(result, guess)
      @output.puts format(@strings['guessResults'][result], guess)
    end

    def display_start_round_header(quantity_guesses_remaining, obfuscated_word)
      @output.puts format(@strings['guesses_remaining'], quantity_guesses_remaining)
      @output.puts format(@strings['trying_to_guess'], obfuscated_word)
    end
  end
end
