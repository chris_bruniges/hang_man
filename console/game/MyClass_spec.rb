class MyClass
  def get_the_thing
    thing
  end

  private

  def thing
    'My internal and complicated thing'
  end
end

describe MyClass do
  subject(:myClass) { MyClass.new }

  describe '#get_the_thing' do
    context 'it returns a complicated value' do
      before do
        myClass.stub(:thing) { 'less complex' }
      end

      it 'returns' do
        expect(myClass.get_the_thing).to eq('less complex')
      end
    end
  end
end

class InjectedClass
  def initialize(dependency)
    @dependency = dependency
  end

  def get_the_thing
    @dependency.thing
  end
end

class ThingHolder
  def thing
    'My internal and complicated thing'
  end
end

describe InjectedClass do
  subject(:myClass) { InjectedClass.new(mock) }
  let(:mock) { instance_double(ThingHolder, {
    :thing => 'less complex'
  })}

  describe '#get_the_thing' do
    context 'it returns a complicated value' do
      it 'returns' do
        expect(myClass.get_the_thing).to eq('less complex')
      end
    end
  end
end
