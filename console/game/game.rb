# frozen_string_literal: true

require './console/game/guess_result'

# Class representing a single Game
class Game
  OBFUSCATION_CHARACTER = '*'

  attr_reader :word

  def initialize(word: false, case_sensitive: true, max_guesses: 5, repository: false)
    @word = word ? word : repository.random_word
    @wrong_letters = []
    @right_letters = []
    @max_guesses = max_guesses
    @case_sensitive = case_sensitive
  end

  def quantity_guesses_remaining
    @max_guesses - @wrong_letters.length
  end

  def obfuscated_word
    result = @word.chars.map do |letter|
      @right_letters.include?(letter) ? letter : OBFUSCATION_CHARACTER
    end
    result.join
  end

  def try_letter(guess)
    sanitized_guess = sanitize?(guess)
    new_value = record(sanitized_guess)
    state?(sanitized_guess, new_value)
  end

  private

  def state?(guess, new_value)
    if !valid_guess?(guess)
      GuessResult::INVALID
    elsif !new_value
      GuessResult::DUPLICATE
    elsif guessed_all_letters?
      GuessResult::FINISHED
    elsif quantity_guesses_remaining.zero?
      GuessResult::FAILED
    elsif word_contains?(guess)
      GuessResult::CORRECT
    else
      GuessResult::INCORRECT
    end
  end

  def sanitize?(guess)
    @case_sensitive ? guess : guess.downcase
  end

  def valid_guess?(guess)
    single_char?(guess) && allowed_character?(guess)
  end

  def single_char?(guess)
    guess.length == 1
  end

  def allowed_character?(guess)
    [*('a'..'z'), *('A'..'Z')].include?(guess)
  end

  def record(letter)
    return if !valid_guess?(letter) || duplicate?(letter)

    if word_contains?(letter)
      record_right_guess(letter)
    else
      record_wrong_guess(letter)
    end
  end

  def word_contains?(letter)
    @word.include?(letter)
  end

  def record_right_guess(letter)
    @right_letters.push(letter)
  end

  def record_wrong_guess(letter)
    @wrong_letters.push(letter)
  end

  def duplicate?(guess)
    @right_letters.include?(guess) || @wrong_letters.include?(guess)
  end

  def guessed_all_letters?
    remaining_letters = remove_guessed_letters(@word, @right_letters)
    remaining_letters.empty?
  end

  def remove_guessed_letters(word, letters)
    letters.each do |letter|
      word = word.delete(letter) while word.include?(letter)
    end
    word
  end
end
