# frozen_string_literal: true

# Runs the game using provided view for one guessable word
module Console
  class GameController
    def initialize(game, view, name, magic_quit_word)
      @game = game
      @view = view
      @name = name
      @magic_quit_word = magic_quit_word
    end

    def run
      take_guess
      @view.display_user_goodbye(@game.word, @name)
    end

    private

    def take_guess
      @view.display_start_round_header(@game.quantity_guesses_remaining, @game.obfuscated_word)

      guess = @view.input_guess_from_user
      return if guess == @magic_quit_word

      result = @game.try_letter(guess)
      @view.display_result(result, guess)

      take_guess unless GuessResult.complete?(result)
    end
  end
end
