# frozen_string_literal: true

# Enum with states after the user has taken a guess
module GuessResult
  INCORRECT = :incorrect
  DUPLICATE = :duplicate
  CORRECT = :correct
  INVALID = :invalid
  FINISHED = :finished
  FAILED = :failed

  def self.complete?(result)
    result == GuessResult::FINISHED || result == GuessResult::FAILED
  end
end
