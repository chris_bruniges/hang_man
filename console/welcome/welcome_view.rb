# frozen_string_literal: true

module Console
  # Generic intro view
  class WelcomeView
    def welcome
      clear_screen
      puts 'Welcome Player1'
    end

    def ask_name
      puts 'Who\'s there?'
      gets.chomp
    end

    private

    def clear_screen
      system 'clear'
    end
  end
end
