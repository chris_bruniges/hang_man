
# frozen_string_literal: true

require './console/game/game'
require './console/game/guess_result'
require './console/game/words_repository'
require './console/game/game_controller'
require './console/game/game_view'
require './console/welcome/welcome_view'

# Container based dependency injection
class Dependencies
  def game(name)
    name.casecmp('hooky') >= 0 ? hooky_game : default_game
  end

  def welcome_view
    WelcomeView.new
  end

  def view(string: strings)
    GameView.new(string)
  end

  def game_controller(name, game_view: view, quit_word: quit_word)
    GameController.new(game(name), game_view, name, quit_word)
  end

  private

  def words_file
    File.new('resources/wordlist_en.txt', 'r')
  end

  def quit_word
    'quit'
  end

  def words_repository(file: words_file)
    WordsRepository.new(file)
  end

  def default_game
    Game.new(repository: words_repository, case_sensitive: false)
  end

  def hooky_game
    Game.new(word: 'frozen', case_sensitive: false)
  end

  def strings
    { 'guessResults' =>
      {
        GuessResult::CORRECT => 'Good guess, contains "%s"',
        GuessResult::INCORRECT => 'Not quite right, try again!',
        GuessResult::INVALID => 'I\'m afraid I can\'t accept "%s"',
        GuessResult::DUPLICATE => 'I think I\'ve seen "%s" before',
        GuessResult::FINISHED => 'Well done, you win!',
        GuessResult::FAILED => 'Too bad, you lose :('
      },
      'prompt' => 'Enter your guess',
      'word_reveal' => "\n\nThe word was %s",
      'bye' => "\n\nBye %s!\n\n",
      'quit' => 'quit',
      'guesses_remaining' => 'You have %d guesses remaining',
      'trying_to_guess' => 'You are trying to guess %s' }
  end
end
