require 'test_helper'

class GameControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get game_index_url
    assert_response :success
  end

  test 'should get index with guess parameter' do
    get game_index_url(guess: 'a')

    assert_response :success
  end

  test 'displays the obfuscated word' do
    get game_index_url(id: 0, guess: 'a')

    assert_select 'h1', 'You are trying to guess ***'
  end

  test 'the word is deobfuscated as you guess some of it' do
    get game_index_url(id: 1, guess: 'b')

    assert_select 'h1', 'You are trying to guess b*b'
  end

  test 'the word is deobfuscated as you guess some more of it' do
    get game_index_url(id: 2, guess: 'b')
    get game_index_url(id: 2, guess: 'o')

    assert_select 'h1', 'You are trying to guess bob'
  end

  test 'winning the game makes a celebration' do
    get game_index_url(id: 3, guess: 'b')
    get game_index_url(id: 3, guess: 'o')

    assert_select 'div[id=win_message]'
    # assert_select 'div[id=lose_message][visibility=hidden]'
  end

  test 'the game id is encoded in the form' do
    get game_index_url(id: 55)

    assert_select 'input' do
      assert_select '[name=?]', 'id'
      assert_select '[value=?]', '55'
    end
  end
end
