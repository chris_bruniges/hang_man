class CorrectWordsTableName < ActiveRecord::Migration[5.2]

  def table_exists?(name)
    ActiveRecord::Base.connection.table_exists? name
  end

  def change
    if table_exists? 'words'
      drop_table 'Words' if table_exists? 'Words'
    else
      rename_table 'Words', 'temp_words'
      rename_table 'temp_words', 'words'
    end
  end
end
