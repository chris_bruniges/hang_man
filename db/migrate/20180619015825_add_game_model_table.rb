class AddGameModelTable < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :word
      t.string :wrong_letters
      t.string :right_letters
      t.integer :max_guesses
      t.boolean :case_sensitive

      t.timestamps
    end
  end
end
