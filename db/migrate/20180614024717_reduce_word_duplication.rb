class ReduceWordDuplication < ActiveRecord::Migration[5.2]
  def change
    rename_column :words, :word, :value
  end
end
