class MoveGuessDataToTurn < ActiveRecord::Migration[5.2]
  def change
    Guess.all.each do |guess|
      Turn.create(
        value: guess.letter,
        game_id: guess.game.id
      )
    end
  end
end
