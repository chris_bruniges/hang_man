Feature: Entering a wrong guess is displayed in the list

Scenario: User starts a new game
Given I am on a new game
When I type in 'z' for a 'letter'
And I click Guess
Then I should see, roughly, I have used z