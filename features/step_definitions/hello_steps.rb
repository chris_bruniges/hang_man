Given(/^I am on the homepage$/) do
  step 'I go to the homepage'
end

Given(/^I am on a new game$/) do
  step 'I go to the homepage'
  step 'I click Start a new game'
end

When(/^I go to the homepage$/) do
  visit root_path
end

When(/^I type in '(\w+)' for a '(\w+)'$/) do |input, field_name|
  text_field = find('input', type: 'text', name: field_name)
  text_field.value = input
end

When("I type in {string} for a {string}") do |input, label_text|
  fill_in(label_text, :with => input)
end

When(/^I click (.+)$/) do |actionable|
  click_on actionable
end

Then(/^I should see (.+)$/) do |content|
  modified = content.gsub 'some quantity of', '\d+'
  modified = modified.gsub /some (.)'s/, '\1+'
  expect(page).to have_content(/#{modified}/i)
end

Then(/^I should be able to click something to (.+)$/) do |actionable|
  element = find('a', text: /#{actionable}/i) || find('button', text: /#{actionable}/i)
  expect(element).not_to be_nil
  element
end

Then(/^I should see, roughly, (I have used z)$/) do |value|
  modified = value.gsub /you /i, '(:?you|i) '
  modified = value.gsub 'I ', '(:?you|i) '
  
end