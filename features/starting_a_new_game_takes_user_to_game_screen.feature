Feature: Starting a game shows you the game screen

Scenario: User starts a new game
Given I am on the homepage
When I click Start a new game
Then I should see you are trying to guess some *'s
And I should see you have some quantity of lives remaining
And I should see you have used { }