# frozen_string_literal: true

# Builds some standard configurations of Games for test purposes
class GameBuilder
  def build_game
    create_game
  end

  def create_game
    game = new_game
    game.save
    game
  end

  def new_game
    Game.new(word: 'bob', max_guesses: @max_guesses || 2)
  end

  def with_max_guesses(quantity)
    @max_guesses = quantity
    self
  end 
end