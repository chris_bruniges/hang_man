require 'rails_helper'

describe WordRepository do
  let(:word) { 'apple' }
  let(:word_response) { "{\"value\":\"devtrain\"}" }
  let(:repo) { WordRepository.new }
  before do
    allow(HTTP).to receive(:get) { word_response }
  end

  describe '@instance' do
    it 'retrieves the same instance each call' do
      expect(WordRepository.instance).to eq(WordRepository.instance)
    end
  end

  describe '#cache_size' do
    it 'is zero initially' do
    end

    it 'increases when we ask for some values' do
    end

    it 'decreases afte we request a word' do
    end
  end

  describe '#fill_cache' do
    it 'hits the service when called' do
      expect(HTTP).to receive(:get) { word_response }
      repo.fill_cache
    end

    it 'returns the new cache size' do
    end
  end

  describe '#request_word' do
    it 'returns a word' do
    end

    context 'has values in cache' do
      it 'does not hit the service' do
      end
    end

    context 'has no values in caches' do
      it 'returns the value after hitting the service' do
      end
    end
  end
end
