require 'rails_helper'
require 'game_guess_helper'
require 'turn'
require 'game'
require 'game_builder'

describe Turn do
  let(:game) { create_ongoing_game }
  let(:second_game) { create_ongoing_game }
  let(:letter) { 'a' }
  let(:builder) { GameBuilder.new }

  def create_ongoing_game
    game = builder.with_max_guesses(5).build_game
    def game.ongoing?
      true
    end
    game
  end

  describe '#initialize' do
    it 'is not null' do
      expect(take_guess(game: game, letter: 'a')).to be_valid
    end

    it 'does not allow multiple characters' do
      expect(take_guess(game: game, letter: 'ab')).not_to be_valid
    end

    it 'does not allow upper case characters' do
      expect(take_guess(game: game, letter: 'A')).not_to be_valid
    end

    it 'does not allow symbols' do
      expect(take_guess(game: game, letter: '$')).not_to be_valid
    end

    context 'when the guess has already been tried for a game' do
      before do
        take_guess(letter: letter, game: game)
      end

      it 'does not allow dublicates' do
        expect(take_guess(game: game, letter: letter)).not_to be_valid
      end

      it 'allows the letter to be used in another game' do
        expect(take_guess(game: second_game, letter: letter)).to be_valid
      end
    end

    context 'when the game has finished' do
      before do
        def game.ongoing?
          false
        end
      end

      it 'does not allow new guesses' do
        expect(take_guess(game: game, letter: letter)).not_to be_valid
      end
    end
  end
end
