require 'rails_helper'
require 'game_guess_helper'
require 'turn'
require 'game'

describe Game do
  let(:game) { Game.create(word: 'bob', max_guesses: max_guesses)}
  let(:failing_wrong_letters) { ['k', 'l']}
  let(:wrong_letter) { failing_wrong_letters.first }
  let(:winning_right_letters) { ['o', 'b']}
  let(:right_letter) { winning_right_letters.first }
  let(:max_guesses) { 2 }

  describe '#initialize' do
    it 'is valid when supplied all the things' do
      expect(Game.new(word: 'bob', max_guesses: 5)).to be_valid
    end

    it 'is not valid without word' do
      expect(Game.new(max_guesses: 5)).not_to be_valid
    end

    it 'is not valid with word containing spaces' do
      expect(Game.new(word: 'asd aas', max_guesses: 5)).not_to be_valid
    end

    it 'is not valid with word containing numbers' do
      expect(Game.new(word: 'asd4aas', max_guesses: 5)).not_to be_valid
    end

    it 'is not valid with word containing other non-letters' do
      expect(Game.new(word: 'asd#aas', max_guesses: 5)).not_to be_valid
    end

    it 'must have multiple characters' do
      expect(Game.new(word: '', max_guesses: 5)).not_to be_valid
    end

    it 'is not valid without max_guesses' do
      expect(Game.new(word: 'bob')).not_to be_valid
    end

    it 'is not valid without positive max_guesses' do
      expect(Game.new(word: 'bob', max_guesses: 0)).not_to be_valid
    end

    it 'does not allow partial max_guesses' do
      expect(Game.new(word: 'bob', max_guesses: 7.345)).not_to be_valid
    end
  end

  describe '#obfuscated_word' do
    context 'have no guessed letters' do
      it 'obfuscates the whole word' do
        expect(game.obfuscated_word).to eq [nil, nil, nil]
      end
    end

    context 'have guessed some incorrect letters' do
      before do
        take_guess(game: game, letter: wrong_letter)
      end

      it 'does not show them' do
        expect(game.obfuscated_word).to eq [nil, nil, nil]
      end
    end

    context 'have guessed some correct letters' do
      before do
        take_guess(game: game, letter: right_letter)
      end

      it 'shows them' do
        expect(game.obfuscated_word).to eq [nil, 'o', nil]
      end
    end
  end

  describe '#quantity_guesses_remaining' do
    context 'have no guessed letters' do
      it 'equals max_guesses' do
        expect(game.quantity_guesses_remaining).to eq max_guesses
      end
    end

    context 'have guessed some correct letters' do
      before do
        take_guess(game: game, letter: right_letter)
      end

      it 'does not affect the guesses remaining' do
        expect(game.quantity_guesses_remaining).to eq max_guesses
      end
    end

    context 'have guessed some incorrect letters' do
      before do
        take_guess(game: game, letter: wrong_letter)
      end

      it 'decrements the guesses remaining' do
        expect(game.quantity_guesses_remaining).to eq (max_guesses - 1)
      end
    end
  end

  describe '#lost?' do
    context 'when you have guessed more than max_guesses wrong' do
      before do
        failing_wrong_letters.each { |l| take_guess(game: game, letter: l) }
      end

      it 'is true' do
        expect(game.lost?).to be true
      end
    end

    context 'when you have not guessed more than max_guesses wrong' do
      before do
        take_guess(game: game, letter: wrong_letter)
      end

      it 'is false' do
        expect(game.lost?).to be false
      end
    end
  end

  describe '#ongoing?' do
    context 'when you have guessed more than max_guesses wrong' do
      before do
        failing_wrong_letters.each { |l| take_guess(game: game, letter: l) }
      end

      it 'is false' do
        expect(game.ongoing?).to be false
      end
    end

    context 'when you have guessed all the letters correctly' do
      before do
        winning_right_letters.each { |l| take_guess(game: game, letter: l) }
      end

      it 'is false' do
        expect(game.ongoing?).to be false
      end
    end

    context 'when you have not guessed more than max_guesses wrong, or guessed all letters' do
      before do
        take_guess(game: game, letter: wrong_letter)
        take_guess(game: game, letter: right_letter)
      end

      it 'is true' do
        expect(game.ongoing?).to be true
      end
    end
  end

  describe '#won?' do
    context 'when you have guessed all the letters correctly' do
      before do
        winning_right_letters.each { |l| take_guess(game: game, letter: l) }
      end

      it 'is true' do
        expect(game.won?).to be true
      end
    end

    context 'when you have not guessed all the letters correctly' do
      before do
        take_guess(game: game, letter: wrong_letter)
        take_guess(game: game, letter: right_letter)
      end

      it 'is false' do
        expect(game.won?).to be false
      end
    end
  end

  describe '#wrong_letters' do
    context 'when you have guessed no letters incorrectly' do
      it 'is an empty array' do
        expect(game.wrong_letters).to eq []
      end
    end

    context 'when you have guessed some letters incorrectly' do
      before do
        take_guess(game: game, letter: wrong_letter)
      end

      it 'adds those letters to the array' do
        expect(game.wrong_letters).to eq [ wrong_letter ]
      end
    end
  end
end