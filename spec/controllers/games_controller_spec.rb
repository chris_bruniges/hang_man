require 'rails_helper'
require 'game_builder'

describe GamesController do
  describe '#index' do
    before { get :index }

    it 'is 200' do
      expect(response).to have_http_status(200)
    end

    it 'assigns a list of games' do
      expect(assigns(:games)).not_to be_nil
    end
  end

  describe '#create' do
    let(:word) { 'frog' }
    let(:word_repository) { instance_double(WordRepository, request_word: word) }

    before do
      allow(WordRepository).to receive(:instance) { word_repository }
    end

    context 'has all necessary things to create a game' do
      before do
        post :create
      end

      it 'should request a word from the repository' do
        expect(word_repository).to have_received(:request_word) { word }
      end

      it 'adds a record to the database when valid' do
        expect(Game.find(1).word).to eq word
      end

      it 'redirects to the show action for that game' do
        expect(response).to redirect_to('/games/1')
      end
    end

    context 'cannot access the external service to retrieve a word' do
      let(:error_message) { 'Some error' }
      before do
        allow(word_repository).to receive(:request_word).and_raise error_message
        post :create
      end

      it 'does not create a new entry in the database' do
        expect(Game.all.count).to eq(0)
      end

      it 'redirects to the index action' do
        expect(response).to redirect_to(:games)
      end

      it 'displays the error message in flash' do
        expect(flash[:notice]).to eq error_message
      end
    end
  end

  describe '#show' do
    context 'there are some pre-existing games' do
      let(:builder) { GameBuilder.new }
      let(:games) { [] }
      before do
        games.push builder.create_game
        games.push builder.create_game
      end

      context 'and the game id is supplied in params' do
        before { get :show, params: { id: 2 } }

        it 'retrieves the game for supplied game id' do
          expect(assigns(:game)).to eq games[1]
        end
      end

      context 'and the game with id is not found' do
        before { get :show, params: { id: 3 } }

        it 'redirects to the index action' do
          expect(response).to redirect_to(:games)
        end

        it 'displays a flash message indicating game doesn\'t exist' do
          expect(flash[:notice]).to match(/Problem running that game/)
        end

        it 'assigns an alternative game id' do
          expect(flash[:alternative_game_id]).not_to be_nil
        end
      end
    end
  end

  describe '#destroy' do
    let(:builder) { GameBuilder.new }

    context 'a valid game id is specefied' do
      let(:valid_game) { builder.create_game }

      before do
        builder.create_game
        delete :destroy, params: { id: valid_game.id }
      end

      it 'looks up the correct game' do
        expect(assigns(:game)).to eq(valid_game)
      end

      it 'deletes the corresponding game' do
        expect(assigns(:game).destroyed?).to be true
      end

      it 'redirects to the index action' do
        expect(response).to redirect_to(:games)
      end

      it 'notifies the user the game was deleted' do
        expect(flash[:notice]).to match(/deleted/i)
      end
    end

    context 'an invalid game id is specefied' do
      before do
        builder.create_game
        delete :destroy, params: { id: 'ab' }
      end

      it 'does not remove any record from the database' do
        expect(Game.count).to eq(1)
      end

      it 'redirects to the index action' do
        expect(response).to redirect_to(:games)
      end
    end
  end
end
