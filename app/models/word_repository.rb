require 'http'

class WordRepository
  @@instance = nil

  def self.instance
    if !@@instance
      @@instance = WordRepository.new
    end
    @@instance
  end

  def request_word
    if cache.any?
      word = cache.pop
    else
      word = word_from_service
    end
    word
  end

  def fill_cache
    5.times do
      cache.push word_from_service
    end
    cache_size
  end

  def cache_size
    cache.count
  end

  private

  def cache
    @cache ||= []
  end

  def word_from_service
    res = HTTP.get('https://hangman-words-service.herokuapp.com/words/random').to_s
    word = JSON.parse res
    word['value'] || 'default'
  end
end
