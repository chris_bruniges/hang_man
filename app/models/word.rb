class Word < ApplicationRecord
  validates :value, presence: { message: 'must not be empty' }
  validates :value, uniqueness: { message: 'can\'t use the same word twice'}
  validates :value, length: {
    in: 3..12,
    too_long: '%{count} characters is the maximum allowed',
    too_short: '%{count} characters is the minimum allowed'
  }
  validates :value, format: {
    with: /\A[a-z]+\z/,
    message: 'only allows lower case letters'
  }

  def self.random
    offset(rand(count)).first
  end
end
