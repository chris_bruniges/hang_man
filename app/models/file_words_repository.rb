# frozen_string_literal: true

# Retrieve words from a \n de-limited file
class FileWordsRepository
  def initialize(file)
    @file = file
  end

  def random_word
    read_words
    max_index = @words.length
    random_index = rand(0..max_index - 1)
    get_word_at(random_index)
  end

  private

  def read_words
    @words = []
    while (line = @file.gets)
      @words.push(line.strip.to_s)
    end
    @file.close
    @words
  end

  def get_word_at(index)
    @words[index]
  end
end
