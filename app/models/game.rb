# frozen_string_literal: true

# Class representing a single Game
class Game < ApplicationRecord
  OBFUSCATION_CHARACTER = '*'

  has_many :turns, dependent: :delete_all
  validates :word, 
    presence: true,
    format: { with: /\A[a-z]*\z/, message: "only allows lower case letters" }
  validates :max_guesses, 
    presence: true,
    numericality: { only_integer: true, greater_than: 0 }

  def quantity_guesses_remaining
    max_guesses - wrong_letters.length
  end

  def obfuscated_word
    word.chars.map do |letter|
      right_letters.include?(letter) ? letter : nil
    end
  end

  def lost?
    quantity_guesses_remaining <= 0
  end

  def ongoing?
    lost? ? false : unguessed_letters? 
  end

  def won?
    !(ongoing? || lost?)
  end

  def wrong_letters
    turns.select { |guess| !guess.correct? }.map { |guess| guess.value }
  end

  private

  def right_letters
    turns.select { |guess| guess.correct? }.map { |guess| guess.value }
  end

  def unguessed_letters?
    obfuscated_word.include? nil
  end
end
