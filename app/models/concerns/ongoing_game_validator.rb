# Prevents new associations with games that have finished

class OngoingGameValidator < ActiveModel::Validator
  def validate(record)
    record.errors.add(:game, "cannot be complete") unless record.game.ongoing?
  end
end
