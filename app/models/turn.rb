class Turn < ApplicationRecord
  belongs_to :game

  validates :game, presence: true
  validates_with OngoingGameValidator
  validates :value,
    length: { is: 1 },
    uniqueness: { scope: :game_id, message: "Have already guessed %{value}" },
    format: { with: /\A[a-z]\z/, message: "only allows lower case" }

  def correct?
    game.word.include? value
  end
end
 