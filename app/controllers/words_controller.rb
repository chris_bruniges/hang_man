
class WordsController < ApplicationController
  def index
    @words = Word.all
  end

  def show
    @word = Word.find(params[:id])
  end

  def create
    word = Word.new(value: params[:word])
    if word.save
      flash[:notice] = "Created \"#{word.value}\""
    else
      flash[:notice] = "Had problems creating \"#{word.value}\" - #{word.errors.messages.first.second.first}"
    end

    redirect_to '/words'
  end

  def destroy
    word = Word.find(params[:id])
    word.destroy
    
    flash[:notice] = "Deleted \"#{word.value}\""
    redirect_to '/words'
  end

  def random
    render :json => Word.random
  end
end
