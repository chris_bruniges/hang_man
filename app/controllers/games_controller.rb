# frozen_string_literal: true

# Interfaces the game logic with the user
class GamesController < ApplicationController

  before_action :set_game, only: %i[update show destroy]

  def index
    @cached_count = "Cached #{WordRepository.instance.cache_size} results"
    @games = Game.all
    id = flash[:alternative_game_id]
    flash[:alternative_game] = id ? Game.find(id) : nil
  end

  def create
    begin
      word = WordRepository.instance.request_word
      game = Game.create(word: word, max_guesses: 5)
      redirect_to game_path game
    rescue StandardError => error
      flash[:notice] = error.message
      redirect_to games_path
    end
  end

  def show
    direct_to_alternative_game unless @game
  end

  def destroy
    if @game
      @game.destroy
      flash[:notice] = 'Deleted your game :('
    end
    redirect_to games_path
  end

  def cache
    WordRepository.instance.fill_cache
    redirect_to games_path
  end

  def update
    if @game
      guessed_letter = params['letter']
      turn = @game.turns.create(value: guessed_letter)
      flash[:notice] = turn.errors.full_messages[0] if turn.errors
      redirect_to game_path @game
    else
      direct_to_alternative_game
    end
  end

  private

  def direct_to_alternative_game
    flash[:alternative_game_id] = random_ongoing_game.id
    message = 'Problem running that game'
    flash[:notice] = message
    redirect_to games_path
  end

  def random_ongoing_game
    alternative_game = Game.all.sample until !Game.any? || (alternative_game && alternative_game.ongoing?)
    alternative_game
  end

  def set_game
    begin
      @game = Game.find(params[:id]) if params[:id]
    rescue
    end
  end
end
