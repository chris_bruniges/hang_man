module ObfuscationHelper
  def obfuscate(game, replacement) 
    game.obfuscated_word.map{ |letter| (letter || replacement) }.join
  end
end