module GuessedLettersHelper
  def guessed_letters(game)
    "{ #{game.wrong_letters.join(', ')} }"
  end
end