module AlternativeGameLinkHelper
  def alternative_game_link(game, alternative)
    return unless game
    obfuscated = obfuscate game, alternative
    link_to "Try #{obfuscated} instead", (game_path game)
  end
end
