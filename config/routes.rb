Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'games#index'
  get 'words/random', to: 'words#random'
  get 'games/cache', to: 'games#cache'
  resources :games
  resources :words
end
